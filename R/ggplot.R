#' Shortcut to add a linear fit and annotations to ggplot
#' @param show_eq Show the fit equation?
#' @param show_rr Show the R^2 value?
#' @param ... Parameters passed to \code{\link[ggplot2]{geom_smooth}}.
#' @return layers
#' @export
linear_fit <- function(show_eq = TRUE, show_rr = TRUE, ...) {
  gs <- ggplot2::geom_smooth(method = "lm", formula = y ~ x, ...)
  # Must be a better way to do this
  if (show_eq && show_rr) {
    spe <- ggpmisc::stat_poly_eq(
      formula = y ~ x, parse = TRUE,
      aes(label = paste(..eq.label.., ..rr.label..,
        sep = "~~~"
      ))
    )
  } else if (show_eq) {
    spe <- ggpmisc::stat_poly_eq(
      formula = y ~ x, parse = TRUE,
      aes(label = ..eq.label..)
    )
  } else if (show_rr) {
    spe <- ggpmisc::stat_poly_eq(
      formula = y ~ x, parse = TRUE,
      aes(label = ..rr.label..)
    )
  } else {
    return(gs)
  }
  return(list(gs, spe))
}
