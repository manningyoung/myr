#' Read from an Excel file where the sheet name is uncertain
#' @param fname Path to Excel file.
#' @param sheet_names List of sheet names to try.
#' @param ... Other arguments to \code{\link[readxl]{read_excel}}.
#' @return tibble
#' @export
read_excel_fuzzy <- function(fname, sheet_names, ...) {
  for (sheet in sheet_names) {
    tryCatch(
      expr = {
        df <- readxl::read_excel(fname, sheet = sheet, ...)
        message("Found sheet named '", sheet, "'")
        return(df)
      },
      error = function(e) {
        NULL
      }
    )
  }
  stop("No matches for sheet_names", call. = FALSE)
}
