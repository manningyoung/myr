% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/vic_api.R
\name{vic_query_measurements}
\alias{vic_query_measurements}
\title{Query measurements from VIC EPA Air Quality API}
\usage{
vic_query_measurements(
  site_id,
  monitor_id = NULL,
  timebasis_id = "1HR_AV",
  start_date = NULL,
  end_date = NULL,
  echo = TRUE
)
}
\arguments{
\item{site_id}{Site ID (see \code{\link{vic_query_sites}}). For now this is
required, to make scripting easier.}

\item{monitor_id}{Monitor ID (see \code{\link{vic_query_monitors}}).}

\item{timebasis_id}{Time basis ID (see \code{\link{vic_query_timebasis}}).
Defaults to hourly averages, "1HR_AV".}

\item{start_date}{Start date to query, as object or YYYYMMDD string.}

\item{end_date}{End date to query, as object or YYYYMMDD string.}

\item{echo}{Print diagnostic information?}
}
\value{
tibble with period-ending date
}
\description{
Query measurements from VIC EPA Air Quality API
}
